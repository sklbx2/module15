#include <iostream>

void printOddOrEvenNumbers(int limit, bool Odd)
{
	for (int i = 0 + Odd; i <= limit; i+=2)
	{
		std::cout << i << std::endl;
	}
}

int main()
{
    const int N = 10;
    
	printOddOrEvenNumbers(N, 1);

	return 0;
}
